import Consumer from 'sqs-consumer';
import AWS from 'aws-sdk';
import SamsungRemote from 'node-samsung-remote';
import Promise from 'bluebird';
import { merge } from 'lutils';
import helpers from './helpers';

class TelevisionCommandProcessor {
  remoteClient;
  sqsClient;

  processedMessages = 0;
  receivedMessages = 0;
  unprocessableMessages = 0;
  sqsClient;
  consumerClient;
  processedMessages = 0;
  receivedMessages = 0;
  unprocessableMessages = 0;
  queueConsumePromise;
  queueTimer;

  config = {
    skipOnProcessingErrors: true,
    authenticationErrorTimeout: 1,
    attributeNames: null,
    messageAttributeNames: null,
    maxProcessedMessages: null,
    visibilityTimeout: 10,
    waitTimeSeconds: 0,
    batchSize: 1,
    maxProcessTime: null,
    pollWhenQueueEmpty: true,
  };

  /**
   * Receives an SQS message object as its first argument. (i.e. `callback(message)`)
   * @callback queueMessageCallback - A function to be called whenever a message is received.
   * @param {Object} message - An SQS Message data object
   * @returns {Promise} - A Promise object
   * @see {@link http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/SQS.html#receiveMessage-property}
   **/

  /**
   * Instantiates a new TelevisionCommandProcessor
   *
   * @param {Object} config - Configuration object for the service
   * @param {string} config.queueUrl - URL to AWS SQS Queue
   * @param {string} config.region - The AWS region to use for SQS
   * @param {queueMessageCallback} config.callback - A function to be called whenever a message
   * is received.
   * @param {!number} [config.authenticationErrorTimeout=1] - The duration (in seconds) to wait
   * before retrying after an authentication error
   * @param {!number} [config.waitTimeSeconds=0] - The duration (in seconds) for which the call
   * will wait for a message to arrive in the queue before returning
   * @param {!number} [config.visibilityTimeout=10] - The duration (in seconds) that the received
   * messages are hidden from subsequent retrieve requests after being retrieved by a
   * ReceiveMessage request
   * @param {?Array} config.attributeNames - List of queue attributes to retrieve (i.e. `['All',
   * 'ApproximateFirstReceiveTimestamp', 'ApproximateReceiveCount']`)
   * @param {?Array} config.messageAttributeNames - List of message attributes to retrieve (i.e.
   * `['name', 'address']`)
   * @param {boolean} [config.skipOnProcessingErrors=false] - Continue processing queue even if a
   * message could not be processed
   * @param {?number} [config.maxProcessTime=30] - Maximum time in seconds to process messages from
   * the queue
   * @param {?number} [config.maxProcessedMessages=1000] - Maximum number of messages to process
   * before stopping consuming more messages from queue
   * @param {!boolean} [config.pollWhenQueueEmpty=false] - Continue to poll queue even if empty
   **/


  constructor(config) {
    this.config = merge(this.config, config);

    AWS.config.credentials = new AWS.SharedIniFileCredentials({
      profile: process.env.AWS_PROFILE,
    });
    this.sqsClient = new AWS.SQS({ region: this.config.aws.region });

    this.remoteClient = new SamsungRemote({
      ip: this.config.tvIpAddress,
      host: {
        ip: '192.168.1.0',
        mac: '00:00:00:00',
        name: 'Amazon Alexa',
      },
      commandInterval: 100,
    });

    this.remoteClient.keys = SamsungRemote.keys;

    try {
      this.consumerClient = Consumer.create({
        authenticationErrorTimeout: this.config.authenticationErrorTimeout * 1000,
        queueUrl: this.config.queueUrl,
        handleMessage: this.process.bind(this),
        sqs: this.sqsClient,
        messageAttributeNames: this.config.messageAttributeNames,
        attributeNames: this.config.attributeNames,
        visibilityTimeout: this.config.visibilityTimeout,
        batchSize: this.config.batchSize,
      });
      // Workaround for sqs-consumer forcing 0 to 20 seconds
      this.consumerClient.waitTimeSeconds = this.config.waitTimeSeconds;
    } catch (e) {
      throw e;
    }

    // Event to throw error on any SQS Errors
    this.consumerClient.on('error', (err) => {
      if (this.queueConsumePromise) {
        this.queueConsumePromise.reject(err);
        this.consumerClient.stop();
      } else {
        throw err;
      }
    });

    // Event to deal with processing errors
    this.consumerClient.on('processing_error', (err) => {
      // console.log('ProcessingError');
      this.unprocessableMessages++;
      // If skip processing errors turned on then do not stop consuming
      if (this.queueConsumePromise && !this.config.skipOnProcessingErrors) {
        this.queueConsumePromise.reject(err);
        this.consumerClient.stop();
      }
    });

    // Event to deal with Queue Empty events
    this.consumerClient.on('empty', () => {
      // console.log('QueueEmpty');
      // Continue polling if queue empty if option set
      if (!this.config.pollWhenQueueEmpty) {
        this.stop();
      }
    });

    // Event to deal with messages received
    this.consumerClient.on('message_received', (msg) => {
      this.receivedMessages++;
    });

    // Event to deal with messages processed by callbacks
    this.consumerClient.on('message_processed', (msg) => {
      this.processedMessages++;
      // Check if total processed messages exceeds any set limits
      // and stop processing queue if limit reached
      if (
        this.config.maxProcessedMessages &&
        this.processedMessages >= this.config.maxProcessedMessages
      ) {
        this.stop();
      }
    });

    // Event to deal with queue processing stopping
    this.consumerClient.on('stopped', () => {
      // console.log('Stopped');
    });
  }


  /**
   * Starts processing queue
   **/

  start() {
    // if queue consume promise exists end it and start new one
    if (this.queueConsumePromise) {
      throw new Error('Cowardly refusing to start queue consumer. Already have started.');
    }

    return new Promise((resolve, reject) => {
      this.queueConsumePromise = { resolve, reject };
      this.consumerClient.start();
      if (this.config.maxProcessTime) {
        this.queueTimer = setTimeout(() => {
          // console.log('ProcessTimeReached');
          this.stop();
        }, this.config.maxProcessTime * 1000);
      }
    });
  }

  /**
   * Stops processing queue
   **/

  stop() {
    if (this.queueConsumePromise) {
      // console.log('stopping');
      this.consumerClient.stop();
      this.queueConsumePromise.resolve();
      this.queueConsumePromise = null;
      if (this.queueTimer) {
        clearTimeout(this.queueTimer);
      }
    }
    return;
  }


  /**
   * Done callback - tells the queue consumer to delete message from queue
   * or to re-queue message if error encountered
   *
   * @callback doneCallback
   * @param {Error} [error] - An optional javascript error message
   **/

  /**
   * Message processing handler/wrapper
   *
   * @access private
   * @param {Object} message - SQS Message Object
   * @param {doneCallback} done - sqs-consumer done callback
   **/

  async process(message, done) {
    try {
      // await this.config.callback(message);
      const context = JSON.parse(message.Body);
      console.log('Message received:', context);
      helpers[context.command](this.remoteClient, context);
      await Promise.delay(300);
      done();
    } catch (e) {
      done(e);
    }
    return;
  }

}

export default TelevisionCommandProcessor;
