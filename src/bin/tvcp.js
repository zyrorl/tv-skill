#!/usr/bin/env node
import fs from 'fs';
import path from 'path';
import Yaml from 'js-yaml';
import yargs from 'yargs';
import TVCP from '../';

const argv = yargs
              .usage('Usage: $0 [options]')
              .option('c', {
                alias: 'config',
                demand: true,
                default: path.join(process.cwd(), './tv-config.yml'),
                describe: 'Config file location',
                nargs: 1,
                type: 'string',
              })
              .help('h')
              .alias('h', 'help')
              .argv;

const config = fs.existsSync(argv.config)
            ? Yaml.load(fs.readFileSync(argv.config))
            : {};


process.env.AWS_PROFILE = config.aws.profile || process.env.AWS_PROFILE || 'default';

const tvProcessor = new TVCP(config);

tvProcessor.start();
