export default class Volume {
  static action(remoteControl, context) {
    switch (context.value) {
      case 'up':
        console.log('Sending volume up');
        remoteControl.send(remoteControl.volumeUp());
        break;
      case 'down':
        console.log('Sending volume down');
        remoteControl.send(remoteControl.volumeDown());
        break;
      default:
        break;
    }
  }
}
