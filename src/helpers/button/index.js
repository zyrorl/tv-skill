export default class Button {
  static action(remoteControl, context) {
    switch (context.value) {
      case 'a':
      case 'b':
      case 'c':
      case 'd':
      case 'blue':
      case 'green':
      case 'red':
      case 'yellow':
        console.log(`Colour button: ${context.value}`);
        remoteControl.colour(context.value);
        break;
      case 'back':
      case 'return':
        console.log('Back button');
        remoteControl.back();
        break;
      case 'exit':
        console.log('Exit button');
        remoteControl.exit();
        break;
      default:
        break;
    }
  }
}
