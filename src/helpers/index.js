import Action from './action';
import Arrow from './arrow';
import Button from './button';
import Channel from './channel';
import Open from './open';
import Power from './power';
import Switch from './switch';
import Volume from './volume';

export default {
  action: Action.action,
  arrow: Arrow.action,
  button: Button.action,
  channel: Channel.action,
  channel_num: Channel.set,
  open: Open.action,
  power: Power.action,
  switch: Switch.action,
  hdmi_num: Switch.hdmi,
  volume: Volume.action,
};
