export default class Channel {
  static action(remoteControl, context) {
    switch (context.value) {
      case 'up':
        console.log('Sending channel up');
        remoteControl.send(remoteControl.channelUp());
        break;
      case 'down':
        console.log('Sending channel down');
        remoteControl.send(remoteControl.channelDown());
        break;
      default:
        break;
    }
  }

  static set(remoteControl, context) {
    console.log(`Setting channel to ${context.value}`);
    remoteControl.send(remoteControl.setChannel(context.value));
  }
}
