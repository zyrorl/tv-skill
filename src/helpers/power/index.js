export default class Power {
  static action(remoteControl, context) {
    switch (context.value) {
      case 'on':
        console.log('Sending power on');
        remoteControl.send(remoteControl.keys.POWERON);
        break;
      case 'off':
        console.log('Sending power off');
        remoteControl.send(remoteControl.keys.POWEROFF);
        break;
      default:
        break;
    }
  }
}
