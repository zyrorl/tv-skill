export default class Action {
  static action(remoteControl, context) {
    switch (context.value) {
      case 'mute':
        console.log('Muting the TV');
        remoteControl.mute(context.value);
        break;
      case 'enter':
      case 'ok':
        console.log('Enter button');
        remoteControl.enter();
        break;
      case 'exit':
        console.log('Exit button');
        remoteControl.exit();
        break;
      case 'back':
      case 'return':
        console.log('Back button');
        remoteControl.back();
        break;
      case 'play':
      case 'pause':
      case 'stop':
      case 'record':
      case 'fast forward':
      case 'rewind':
      case 'previous':
      case 'next':
        switch (context.value) {
          case 'rewind':
            context.value = 'backward';
            break;
          case 'fast forward':
            context.value = 'forward';
            break;
          case 'next':
            context.value = 'skip-forward';
            break;
          case 'previous':
            context.value = 'skip-backward';
            break;
          default:
            break;
        }
        console.log('the context: ', context.value);
        remoteControl.transport(context.value);
        break;
      default:
        break;
    }
  }
}
