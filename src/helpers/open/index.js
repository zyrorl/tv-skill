export default class Open {
  static action(remoteControl, context) {
    switch (context.value) {
      // GUIDE | MENU | TOOLS | INFO | CHANNEL LIST
      case 'guide':
        console.log('Opening guide');
        remoteControl.guide();
        break;
      case 'menu':
        console.log('Opening Menu');
        remoteControl.menu();
        break;
      case 'tools':
        console.log('Opening Tools');
        remoteControl.tools();
        break;
      case 'smart hub':
        console.log('Opening Smart Hub');
        remoteControl.smartHub();
        break;
      case 'info':
        console.log('Opening Info');
        remoteControl.info();
        break;
      case 'channel list':
        remoteControl.send(remoteControl.keys.CH_LIST);
        break;
      default:
        break;
    }
  }
}
