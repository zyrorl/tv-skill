export default class Switch {
  static action(remoteControl, context) {
    switch (context.value) {
      case 'previous channel':
        console.log('Switching to previous channel');
        remoteControl.previousChannel();
        break;
      case 'TV':
        console.log('Switching to tv');
        remoteControl.tv();
        break;
      case 'HDMI':
        console.log('Switching to HDMI');
        remoteControl.hdmi();
        break;
      case 'source':
        console.log('Switching source');
        remoteControl.source();
        remoteControl.source();
        break;
      case 'off':
        console.log('Switching TV Off');
        remoteControl.send(remoteControl.keys.POWEROFF);
        break;
      case 'channel list':
        remoteControl.send(remoteControl.keys.CH_LIST);
        break;
      default:
        break;
    }
  }
  static hdmi(remoteControl, context) {
    console.log('pressing key: ', remoteControl.keys[`HDMI${context.value}`]);
    remoteControl.send(remoteControl.keys[`HDMI${context.value}`]);
  }
}
