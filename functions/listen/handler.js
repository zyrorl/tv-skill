import 'source-map-support/register';
import Alexa from 'alexa-sdk';
import moment from 'moment';
import AWS from 'aws-sdk';
import Promise from 'bluebird';

const SQS = Promise.promisifyAll(new AWS.SQS({
  region: process.env.SERVERLESS_REGION,
}));

const handlers = {
  LaunchRequest() {
    this.emit('TVControlIntent');
  },
  async TVControlIntent() {
    // console.log(JSON.stringify(this.context));
    // console.log(JSON.stringify(this.event));

    const slots = this.event.request.intent.slots;
    const emit = this.emit;

    console.log(JSON.stringify(this.event.request));
    console.log(JSON.stringify(slots));

    let queueMessage;

    for (const x in slots) {
      const slot = slots[x];
      if (slot.value) {
        queueMessage = {
          command: slot.name,
          value: slot.value,
          timestamp: moment().toISOString(),
        };

        if (queueMessage) {
          const result = await SQS.sendMessageAsync({
            QueueUrl: process.env.COMMAND_QUEUE_URL,
            MessageBody: JSON.stringify(queueMessage),
          });
          console.log(result);
        }
        console.log('done');

        switch (slot.name.toLowerCase()) {
          case 'volume':
            emit(':tell', `Turning the volume ${slot.value}`);
            break;
          case 'channel':
            emit(':tell', `Channel ${slot.value}`);
            break;
          case 'channel_num':
            emit(':tell', `Changing to channel ${slot.value}`);
            break;
          case 'hdmi_num':
            emit(':tell', `Switching to HDMI ${slot.value}`);
            break;
          case 'button':
            emit(':tell', `Pressing ${slot.value} button`);
            break;
          case 'arrow':
            emit(':tell', `Arrow ${slot.value}`);
            break;
          case 'open':
            emit(':tell', `Opening ${slot.value}`);
            break;
          case 'switch':
            emit(':tell', `Switching to ${slot.value}`);
            break;
          case 'action':
            switch (slot.value.toLowerCase()) {
              case 'mute':
                emit(':tell', 'Muting the TV');
                break;
              case 'exit':
                emit(':tell', 'Exiting');
                break;
              case 'return':
                emit(':tell', 'Returning');
                break;
              default:
                emit(':tell', `${slot.value} TV`);
                break;
            }
            break;
          case 'power':
            emit(':tell', `Turning the TV ${slot.value}`);
            break;
          default:
            emit(':tell', 'I did not understand your instruction');
        }
      }
    }
  },
  Unhandled() {
    this.emit(':ask', 'Sorry, I didn\'t get that. Try saying an action.', 'Try saying an action');
  },
};

exports.handler = function (event, context, callback) {
  const alexa = Alexa.handler(event, context);
  alexa.appId = 'amzn1.ask.skill.00543d95-28b4-4960-a758-28679fad0ef5';
  alexa.registerHandlers(handlers);
  alexa.execute();
};
