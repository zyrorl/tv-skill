'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _sqsConsumer = require('sqs-consumer');

var _sqsConsumer2 = _interopRequireDefault(_sqsConsumer);

var _awsSdk = require('aws-sdk');

var _awsSdk2 = _interopRequireDefault(_awsSdk);

var _nodeSamsungRemote = require('node-samsung-remote');

var _nodeSamsungRemote2 = _interopRequireDefault(_nodeSamsungRemote);

var _lutils = require('lutils');

var _helpers = require('./helpers');

var _helpers2 = _interopRequireDefault(_helpers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TelevisionCommandProcessor = function () {

  /**
   * Receives an SQS message object as its first argument. (i.e. `callback(message)`)
   * @callback queueMessageCallback - A function to be called whenever a message is received.
   * @param {Object} message - An SQS Message data object
   * @returns {Promise} - A Promise object
   * @see {@link http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/SQS.html#receiveMessage-property}
   **/

  /**
   * Instantiates a new TelevisionCommandProcessor
   *
   * @param {Object} config - Configuration object for the service
   * @param {string} config.queueUrl - URL to AWS SQS Queue
   * @param {string} config.region - The AWS region to use for SQS
   * @param {queueMessageCallback} config.callback - A function to be called whenever a message
   * is received.
   * @param {!number} [config.authenticationErrorTimeout=1] - The duration (in seconds) to wait
   * before retrying after an authentication error
   * @param {!number} [config.waitTimeSeconds=0] - The duration (in seconds) for which the call
   * will wait for a message to arrive in the queue before returning
   * @param {!number} [config.visibilityTimeout=10] - The duration (in seconds) that the received
   * messages are hidden from subsequent retrieve requests after being retrieved by a
   * ReceiveMessage request
   * @param {?Array} config.attributeNames - List of queue attributes to retrieve (i.e. `['All',
   * 'ApproximateFirstReceiveTimestamp', 'ApproximateReceiveCount']`)
   * @param {?Array} config.messageAttributeNames - List of message attributes to retrieve (i.e.
   * `['name', 'address']`)
   * @param {boolean} [config.skipOnProcessingErrors=false] - Continue processing queue even if a
   * message could not be processed
   * @param {?number} [config.maxProcessTime=30] - Maximum time in seconds to process messages from
   * the queue
   * @param {?number} [config.maxProcessedMessages=1000] - Maximum number of messages to process
   * before stopping consuming more messages from queue
   * @param {!boolean} [config.pollWhenQueueEmpty=false] - Continue to poll queue even if empty
   **/

  function TelevisionCommandProcessor(config) {
    var _this = this;

    (0, _classCallCheck3.default)(this, TelevisionCommandProcessor);
    this.processedMessages = 0;
    this.receivedMessages = 0;
    this.unprocessableMessages = 0;
    this.processedMessages = 0;
    this.receivedMessages = 0;
    this.unprocessableMessages = 0;
    this.config = {
      skipOnProcessingErrors: true,
      authenticationErrorTimeout: 1,
      attributeNames: null,
      messageAttributeNames: null,
      maxProcessedMessages: null,
      visibilityTimeout: 10,
      waitTimeSeconds: 0,
      batchSize: 1,
      maxProcessTime: null,
      pollWhenQueueEmpty: true
    };

    this.config = (0, _lutils.merge)(this.config, config);

    _awsSdk2.default.config.credentials = new _awsSdk2.default.SharedIniFileCredentials({
      profile: process.env.AWS_PROFILE
    });
    this.sqsClient = new _awsSdk2.default.SQS({ region: this.config.aws.region });

    this.remoteClient = new _nodeSamsungRemote2.default({
      ip: this.config.tvIpAddress,
      host: {
        ip: '192.168.1.0',
        mac: '00:00:00:00',
        name: 'Amazon Alexa'
      },
      commandInterval: 100
    });

    this.remoteClient.keys = _nodeSamsungRemote2.default.keys;

    try {
      this.consumerClient = _sqsConsumer2.default.create({
        authenticationErrorTimeout: this.config.authenticationErrorTimeout * 1000,
        queueUrl: this.config.queueUrl,
        handleMessage: this.process.bind(this),
        sqs: this.sqsClient,
        messageAttributeNames: this.config.messageAttributeNames,
        attributeNames: this.config.attributeNames,
        visibilityTimeout: this.config.visibilityTimeout,
        batchSize: this.config.batchSize
      });
      // Workaround for sqs-consumer forcing 0 to 20 seconds
      this.consumerClient.waitTimeSeconds = this.config.waitTimeSeconds;
    } catch (e) {
      throw e;
    }

    // Event to throw error on any SQS Errors
    this.consumerClient.on('error', function (err) {
      if (_this.queueConsumePromise) {
        _this.queueConsumePromise.reject(err);
        _this.consumerClient.stop();
      } else {
        throw err;
      }
    });

    // Event to deal with processing errors
    this.consumerClient.on('processing_error', function (err) {
      // console.log('ProcessingError');
      _this.unprocessableMessages++;
      // If skip processing errors turned on then do not stop consuming
      if (_this.queueConsumePromise && !_this.config.skipOnProcessingErrors) {
        _this.queueConsumePromise.reject(err);
        _this.consumerClient.stop();
      }
    });

    // Event to deal with Queue Empty events
    this.consumerClient.on('empty', function () {
      // console.log('QueueEmpty');
      // Continue polling if queue empty if option set
      if (!_this.config.pollWhenQueueEmpty) {
        _this.stop();
      }
    });

    // Event to deal with messages received
    this.consumerClient.on('message_received', function (msg) {
      _this.receivedMessages++;
    });

    // Event to deal with messages processed by callbacks
    this.consumerClient.on('message_processed', function (msg) {
      _this.processedMessages++;
      // Check if total processed messages exceeds any set limits
      // and stop processing queue if limit reached
      if (_this.config.maxProcessedMessages && _this.processedMessages >= _this.config.maxProcessedMessages) {
        _this.stop();
      }
    });

    // Event to deal with queue processing stopping
    this.consumerClient.on('stopped', function () {
      // console.log('Stopped');
    });
  }

  /**
   * Starts processing queue
   **/

  (0, _createClass3.default)(TelevisionCommandProcessor, [{
    key: 'start',
    value: function start() {
      var _this2 = this;

      // if queue consume promise exists end it and start new one
      if (this.queueConsumePromise) {
        throw new Error('Cowardly refusing to start queue consumer. Already have started.');
      }

      return new _bluebird2.default(function (resolve, reject) {
        _this2.queueConsumePromise = { resolve: resolve, reject: reject };
        _this2.consumerClient.start();
        if (_this2.config.maxProcessTime) {
          _this2.queueTimer = setTimeout(function () {
            // console.log('ProcessTimeReached');
            _this2.stop();
          }, _this2.config.maxProcessTime * 1000);
        }
      });
    }

    /**
     * Stops processing queue
     **/

  }, {
    key: 'stop',
    value: function stop() {
      if (this.queueConsumePromise) {
        // console.log('stopping');
        this.consumerClient.stop();
        this.queueConsumePromise.resolve();
        this.queueConsumePromise = null;
        if (this.queueTimer) {
          clearTimeout(this.queueTimer);
        }
      }
      return;
    }

    /**
     * Done callback - tells the queue consumer to delete message from queue
     * or to re-queue message if error encountered
     *
     * @callback doneCallback
     * @param {Error} [error] - An optional javascript error message
     **/

    /**
     * Message processing handler/wrapper
     *
     * @access private
     * @param {Object} message - SQS Message Object
     * @param {doneCallback} done - sqs-consumer done callback
     **/

  }, {
    key: 'process',
    value: function () {
      var _ref = (0, _bluebird.coroutine)(_regenerator2.default.mark(function _callee(message, done) {
        var context;
        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;

                // await this.config.callback(message);
                context = JSON.parse(message.Body);

                console.log('Message received:', context);
                _helpers2.default[context.command](this.remoteClient, context);
                _context.next = 6;
                return _bluebird2.default.delay(300);

              case 6:
                done();
                _context.next = 12;
                break;

              case 9:
                _context.prev = 9;
                _context.t0 = _context['catch'](0);

                done(_context.t0);

              case 12:
                return _context.abrupt('return');

              case 13:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 9]]);
      }));

      function process(_x, _x2) {
        return _ref.apply(this, arguments);
      }

      return process;
    }()
  }]);
  return TelevisionCommandProcessor;
}();

exports.default = TelevisionCommandProcessor;
module.exports = exports['default'];