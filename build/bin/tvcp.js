#!/usr/bin/env node
'use strict';

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _jsYaml = require('js-yaml');

var _jsYaml2 = _interopRequireDefault(_jsYaml);

var _yargs = require('yargs');

var _yargs2 = _interopRequireDefault(_yargs);

var _ = require('../');

var _2 = _interopRequireDefault(_);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var argv = _yargs2.default.usage('Usage: $0 [options]').option('c', {
  alias: 'config',
  demand: true,
  default: _path2.default.join(process.cwd(), './tv-config.yml'),
  describe: 'Config file location',
  nargs: 1,
  type: 'string'
}).help('h').alias('h', 'help').argv;

var config = _fs2.default.existsSync(argv.config) ? _jsYaml2.default.load(_fs2.default.readFileSync(argv.config)) : {};

process.env.AWS_PROFILE = config.aws.profile || process.env.AWS_PROFILE || 'default';

var tvProcessor = new _2.default(config);

tvProcessor.start();