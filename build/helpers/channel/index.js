'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Channel = function () {
  function Channel() {
    (0, _classCallCheck3.default)(this, Channel);
  }

  (0, _createClass3.default)(Channel, null, [{
    key: 'action',
    value: function action(remoteControl, context) {
      switch (context.value) {
        case 'up':
          console.log('Sending channel up');
          remoteControl.send(remoteControl.channelUp());
          break;
        case 'down':
          console.log('Sending channel down');
          remoteControl.send(remoteControl.channelDown());
          break;
        default:
          break;
      }
    }
  }, {
    key: 'set',
    value: function set(remoteControl, context) {
      console.log('Setting channel to ' + context.value);
      remoteControl.send(remoteControl.setChannel(context.value));
    }
  }]);
  return Channel;
}();

exports.default = Channel;
module.exports = exports['default'];