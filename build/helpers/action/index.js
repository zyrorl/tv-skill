'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Action = function () {
  function Action() {
    (0, _classCallCheck3.default)(this, Action);
  }

  (0, _createClass3.default)(Action, null, [{
    key: 'action',
    value: function action(remoteControl, context) {
      switch (context.value) {
        case 'mute':
          console.log('Muting the TV');
          remoteControl.mute(context.value);
          break;
        case 'enter':
        case 'ok':
          console.log('Enter button');
          remoteControl.enter();
          break;
        case 'exit':
          console.log('Exit button');
          remoteControl.exit();
          break;
        case 'back':
        case 'return':
          console.log('Back button');
          remoteControl.back();
          break;
        case 'play':
        case 'pause':
        case 'stop':
        case 'record':
        case 'fast forward':
        case 'rewind':
        case 'previous':
        case 'next':
          switch (context.value) {
            case 'rewind':
              context.value = 'backward';
              break;
            case 'fast forward':
              context.value = 'forward';
              break;
            case 'next':
              context.value = 'skip-forward';
              break;
            case 'previous':
              context.value = 'skip-backward';
              break;
            default:
              break;
          }
          console.log('the context: ', context.value);
          remoteControl.transport(context.value);
          break;
        default:
          break;
      }
    }
  }]);
  return Action;
}();

exports.default = Action;
module.exports = exports['default'];