'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Volume = function () {
  function Volume() {
    (0, _classCallCheck3.default)(this, Volume);
  }

  (0, _createClass3.default)(Volume, null, [{
    key: 'action',
    value: function action(remoteControl, context) {
      switch (context.value) {
        case 'up':
          console.log('Sending volume up');
          remoteControl.send(remoteControl.volumeUp());
          break;
        case 'down':
          console.log('Sending volume down');
          remoteControl.send(remoteControl.volumeDown());
          break;
        default:
          break;
      }
    }
  }]);
  return Volume;
}();

exports.default = Volume;
module.exports = exports['default'];