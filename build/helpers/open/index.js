'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Open = function () {
  function Open() {
    (0, _classCallCheck3.default)(this, Open);
  }

  (0, _createClass3.default)(Open, null, [{
    key: 'action',
    value: function action(remoteControl, context) {
      switch (context.value) {
        // GUIDE | MENU | TOOLS | INFO | CHANNEL LIST
        case 'guide':
          console.log('Opening guide');
          remoteControl.guide();
          break;
        case 'menu':
          console.log('Opening Menu');
          remoteControl.menu();
          break;
        case 'tools':
          console.log('Opening Tools');
          remoteControl.tools();
          break;
        case 'smart hub':
          console.log('Opening Smart Hub');
          remoteControl.smartHub();
          break;
        case 'info':
          console.log('Opening Info');
          remoteControl.info();
          break;
        case 'channel list':
          remoteControl.send(remoteControl.keys.CH_LIST);
          break;
        default:
          break;
      }
    }
  }]);
  return Open;
}();

exports.default = Open;
module.exports = exports['default'];