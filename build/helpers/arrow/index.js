'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Arrow = function () {
  function Arrow() {
    (0, _classCallCheck3.default)(this, Arrow);
  }

  (0, _createClass3.default)(Arrow, null, [{
    key: 'action',
    value: function action(remoteControl, context) {
      console.log('the context', context);
      remoteControl.arrow(context.value);
    }
  }]);
  return Arrow;
}();

exports.default = Arrow;
module.exports = exports['default'];