'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Button = function () {
  function Button() {
    (0, _classCallCheck3.default)(this, Button);
  }

  (0, _createClass3.default)(Button, null, [{
    key: 'action',
    value: function action(remoteControl, context) {
      switch (context.value) {
        case 'a':
        case 'b':
        case 'c':
        case 'd':
        case 'blue':
        case 'green':
        case 'red':
        case 'yellow':
          console.log('Colour button: ' + context.value);
          remoteControl.colour(context.value);
          break;
        case 'back':
        case 'return':
          console.log('Back button');
          remoteControl.back();
          break;
        case 'exit':
          console.log('Exit button');
          remoteControl.exit();
          break;
        default:
          break;
      }
    }
  }]);
  return Button;
}();

exports.default = Button;
module.exports = exports['default'];