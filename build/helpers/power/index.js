'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Power = function () {
  function Power() {
    (0, _classCallCheck3.default)(this, Power);
  }

  (0, _createClass3.default)(Power, null, [{
    key: 'action',
    value: function action(remoteControl, context) {
      switch (context.value) {
        case 'on':
          console.log('Sending power on');
          remoteControl.send(remoteControl.keys.POWERON);
          break;
        case 'off':
          console.log('Sending power off');
          remoteControl.send(remoteControl.keys.POWEROFF);
          break;
        default:
          break;
      }
    }
  }]);
  return Power;
}();

exports.default = Power;
module.exports = exports['default'];