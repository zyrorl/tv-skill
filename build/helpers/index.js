'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _action = require('./action');

var _action2 = _interopRequireDefault(_action);

var _arrow = require('./arrow');

var _arrow2 = _interopRequireDefault(_arrow);

var _button = require('./button');

var _button2 = _interopRequireDefault(_button);

var _channel = require('./channel');

var _channel2 = _interopRequireDefault(_channel);

var _open = require('./open');

var _open2 = _interopRequireDefault(_open);

var _power = require('./power');

var _power2 = _interopRequireDefault(_power);

var _switch = require('./switch');

var _switch2 = _interopRequireDefault(_switch);

var _volume = require('./volume');

var _volume2 = _interopRequireDefault(_volume);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  action: _action2.default.action,
  arrow: _arrow2.default.action,
  button: _button2.default.action,
  channel: _channel2.default.action,
  channel_num: _channel2.default.set,
  open: _open2.default.action,
  power: _power2.default.action,
  switch: _switch2.default.action,
  hdmi_num: _switch2.default.hdmi,
  volume: _volume2.default.action
};
module.exports = exports['default'];