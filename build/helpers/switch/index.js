'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Switch = function () {
  function Switch() {
    (0, _classCallCheck3.default)(this, Switch);
  }

  (0, _createClass3.default)(Switch, null, [{
    key: 'action',
    value: function action(remoteControl, context) {
      switch (context.value) {
        case 'previous channel':
          console.log('Switching to previous channel');
          remoteControl.previousChannel();
          break;
        case 'TV':
          console.log('Switching to tv');
          remoteControl.tv();
          break;
        case 'HDMI':
          console.log('Switching to HDMI');
          remoteControl.hdmi();
          break;
        case 'source':
          console.log('Switching source');
          remoteControl.source();
          remoteControl.source();
          break;
        case 'off':
          console.log('Switching TV Off');
          remoteControl.send(remoteControl.keys.POWEROFF);
          break;
        case 'channel list':
          remoteControl.send(remoteControl.keys.CH_LIST);
          break;
        default:
          break;
      }
    }
  }, {
    key: 'hdmi',
    value: function hdmi(remoteControl, context) {
      console.log('pressing key: ', remoteControl.keys['HDMI' + context.value]);
      remoteControl.send(remoteControl.keys['HDMI' + context.value]);
    }
  }]);
  return Switch;
}();

exports.default = Switch;
module.exports = exports['default'];